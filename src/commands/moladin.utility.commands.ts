export class MoladinUtilityCommand {
    async delay(timeout:number) {
        await driver.pause(timeout);
    }

    async sendKeysEvent(keycode:string) {
        await driver.sendKeyEvent(keycode);
    }

    async backPage() {
        await driver.back();
    }

    async sessionReload() {
        const session: any = await driver.getSession();
        const isEmpty: boolean = Object.keys(session).length === 0;

        if (!isEmpty) await driver.reloadSession();
    }

    async relaunchApps() {
        await driver.startActivity('com.moladin.moladinagent.debug', 'com.moladin.moladinagent.module.splash.SplashActivity');
    }

    async startActivity(appPackage:string, appActivity:string) {
        await driver.startActivity(appPackage, appActivity);
    }

    async acceptAlert(buttonTextOrLabel:string) {
        const options = buttonTextOrLabel ? {
            buttonLabel: buttonTextOrLabel,
        } : {};
        await driver.execute('mobile: acceptAlert', options);
    }

    async dismissAlert(buttonTextOrLabel:string) {
        const options = buttonTextOrLabel ? {
            buttonLabel: buttonTextOrLabel,
        } : {};
        await driver.execute('mobile: dismissAlert', options);
    }

    async pushFileToDevice(pathFile:string, base64ImageData:string) {
        await driver.pushFile(pathFile, base64ImageData);
    }

    async deleteFileFromDevice(remotePathFile:string) {
        const options = remotePathFile ? {
            remotePath: remotePathFile,
        } : {};
        await driver.execute('mobile: deleteFile', options);
    }

    async openDeepLink(urlDeeplink:string) {
        const options = {
            url: urlDeeplink,
            package: 'com.moladin.moladinagent.debug',
        };
        await driver.execute('mobile: deepLink', options);
    }

    async executeADBShell(commandInput:string) {
        const options = {
            command: commandInput,
        };
        await driver.execute('mobile: shell', options);
    }

    // async takeScreenShotAndSaveToFile(fileName:string, pathToFolder:string) {
    //     const base64ImageData: any = await driver.takeScreenshot();

    //     await fs.writeFile(`${pathToFolder}/${fileName}`, base64ImageData );
    // }
}