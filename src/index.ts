import Utils from "./helpers/utils";
import Commands from "./commands";

class MojaveEvo {
    utils = Utils;
    commands = Commands;
}

export default new MojaveEvo();
